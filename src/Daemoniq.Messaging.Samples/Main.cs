using System;

namespace Daemoniq.Messaging.Samples
{
	class MainClass
	{
		public static DummyQueue<int> dummyQueue = new DummyQueue<int>();
		public static DumbWorker dummyWorker = new DumbWorker();
		
		public static void Main (string[] args)
		{
			Console.WriteLine ("Press any key to start dummy worker...");
			Console.ReadKey(true);
			dummyWorker.Start();
			
			Console.WriteLine ("Press any key to start dummy queue...");
			Console.ReadKey(true);
			dummyWorker.Queue = dummyQueue;
			dummyQueue.Start();
			
			Console.WriteLine ("Press any key to exit...");
			Console.ReadKey(true);
			dummyWorker.Stop();
			dummyWorker.Queue = null;
			dummyWorker = null;
			dummyQueue.Stop();	

//			Console.WriteLine ("Press any key to start...");
//			Console.ReadKey(true);
//		
//			using(Dispatcher dispatcher = new Dispatcher(2))
//			{
//				Pong pong = new Pong();
//				Ping ping = new Ping(100000000, pong);
//				dispatcher.Register(pong);
//				dispatcher.Register(ping);
//				dispatcher.Start();	
//				
//				Console.WriteLine ("Press any key to exit...");
//				Console.ReadKey(true);
//			}
//			Console.WriteLine ("Program Finished.");
		}
	}
}