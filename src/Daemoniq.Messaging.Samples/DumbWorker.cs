using System;

namespace Daemoniq.Messaging.Samples
{
	public class DumbWorker : WorkerThread
	{
		public ThreadedQueue<int> Queue {
			get;
			set;
		}
		
		protected override void Dispose (bool disposing)
		{
			Console.WriteLine ("Disposing {0}={1}", GetType().Name, disposing);
			base.Dispose (disposing);
		}
		
		public override void Work()
		{
			Console.WriteLine ("Producer started.");
			int count = 0;
			while(!StopRequested)
			{
				ThreadedQueue<int> queue = Queue;
				if(queue != null)
				{				
					Console.WriteLine ("Sending {0}...", count);
					queue.Enqueue(count);
					count++;
				}
				
				if(count == 1000000)
				{
					break;
				}
			}
		}
	}
}