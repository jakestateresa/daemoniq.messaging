using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Daemoniq.Messaging.Samples
{
	public class Ping:MessageProcessor
	{
		private int _pingsLeft;
		private MessageProcessor _pong;
		private Stopwatch sw = new System.Diagnostics.Stopwatch();
		
		public Ping(int count, MessageProcessor pong)
		{
			Console.WriteLine (count);
			_pingsLeft = count - 1;
			_pong = pong;
		}
		
		public override IEnumerable<Type> ContentTypes
		{
			get
			{
				yield return typeof(string);
			}
		}
		
		protected override void start ()
		{
			sw.Start();
			Console.WriteLine ("Ping Started.");
			Message pingMessage = new Message(Address, _pong.Address, "ping");
			Send(pingMessage);
		}
		
		protected override void stop ()
		{
			Console.WriteLine ("Ping Stopped");
		}
		
		protected override void receive (IMessage message)
		{
			string messageBody = message.Body.ToString();
			if(messageBody == "pong")
			{
				if (_pingsLeft % 1000 == 0) 
				{
					Console.WriteLine ("Ping: pong {0}", _pingsLeft);
				}	

				if (_pingsLeft > 0) 
				{
		            Message pingMessage = new Message(Address, _pong.Address, "ping");
					Send(pingMessage);
					_pingsLeft--;
				}
				else
				{
					
					sw.Stop();
					Console.WriteLine ("Operation took {0}ms", sw.ElapsedMilliseconds);
				}
			}
		}
	}
}