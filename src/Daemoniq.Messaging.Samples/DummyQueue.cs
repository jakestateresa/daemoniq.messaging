using System;

namespace Daemoniq.Messaging.Samples
{
	public class DummyQueue<T> : ThreadedQueue<T>
	{
		protected override void Process (T item)
		{			
			Console.WriteLine ("Received: {0}", item);
		}
		
		protected override void Dispose (bool disposing)
		{
			Console.WriteLine ("Disposing {0}={1}", GetType().Name, disposing);
			base.Dispose (disposing);
		}
	}
}