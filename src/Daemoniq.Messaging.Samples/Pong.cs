using System;
using System.Collections.Generic;

namespace Daemoniq.Messaging.Samples
{
	public class Pong:MessageProcessor
	{
		private int _pongCount;
		
		public override IEnumerable<Type> ContentTypes
		{
			get
			{
				yield return typeof(string);
			}
		}
		
		protected override void start ()
		{
			Console.WriteLine ("Pong Started.");
		}
		
		protected override void stop ()
		{
			Console.WriteLine ("Pong Stopped");
		}
		
		protected override void receive (IMessage message)
		{
			string messageBody = message.Body.ToString();
			if(messageBody == "ping")
			{
				if (_pongCount % 1000 == 0) 
				{
					Console.WriteLine ("Pong: ping {0}", _pongCount);
				}	
				
	            Message pongMessage = new Message(message.Header.Sender, "pong");
				Send(pongMessage);
				_pongCount++;	
			}
		}
	}
}