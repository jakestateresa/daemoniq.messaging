using System;

namespace Daemoniq.Messaging
{
	public interface IMessage
	{
		MessageHeader Header { get; }
		object Body{ get; }
	}
	
	public class Message:IMessage
	{
		private MessageHeader _header;
		private object _body;
		
		public Message(string recepient, object body)
			: this(null, recepient, body)
		{
		}
		
		public Message(string sender, string recepient, object body)
		{
			_header = new MessageHeader(sender, recepient);
			
			if(null == body) throw new ArgumentNullException("body");			
			
			_body = body;
		}
		
		public MessageHeader Header		
		{
			get { return _header; }
		}
		
		public object Body
		{
			get { return _body; }			
		}
	}
}