using System;
using System.Collections.Generic;

namespace Daemoniq.Messaging
{	
	public abstract class MessageProcessor
	{	
		private Dispatcher _dispatcher;
		private IList<Type> _contentTypes;
		
		public MessageProcessor()
		{
			Address = GetType().FullName;
			Status = Status.Stopped;
		}
		
		public MessageProcessor(string address)
		{
			Address = address;
			Status = Status.Stopped;
		}
		
		protected internal Dispatcher Dispatcher
		{
			get { return _dispatcher; }	
			set { _dispatcher = value; }
		}
		
		public void Send(IMessage message)
		{
			if(null == message) throw new ArgumentNullException("message");
			if(Dispatcher == null)
			{
				throw new InvalidOperationException();
			}
			Dispatcher.Enqueue(message);
		}
		
		public void Receive(IMessage message)
		{
			if(null == message) throw new ArgumentNullException("message");
			if(Dispatcher == null)
			{
				throw new InvalidOperationException();
			}
			receive(message);
		}
			
		public bool CanAccept(Type type)
		{
			if(null == type) throw new ArgumentNullException("type");
			
			if(_contentTypes == null)
			{
				_contentTypes = new List<Type>(ContentTypes).AsReadOnly();
			}
			return _contentTypes.Contains(type);
		}
		
		public void Start()
		{	
			Status = Status.Started;
			Console.WriteLine ("Listening on {0}", Address);
			start();
		}
		
		public void Stop()
		{
			Status = Status.Stopped;
			Console.WriteLine ("Stopping listening on {0}", Address);
			stop();
		}
		
		public void Pause()
		{
			Status = Status.Paused;
			pause();
		}
		
		protected virtual void start(){}
		protected virtual void stop(){}
		protected virtual void pause(){}
		
		public Status Status { get; internal set;}		
		public string Address { get; internal set;}		
		public abstract IEnumerable<Type> ContentTypes { get; }	
		protected abstract void receive(IMessage message);
	}
}