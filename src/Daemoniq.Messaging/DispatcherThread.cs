using System;
using System.Threading;

namespace Daemoniq.Messaging
{
	class DispatcherThread:ThreadedQueue<IMessage>		
	{
		private Dispatcher _dispatcher;
		
		public DispatcherThread(string name)
			:base(name, ThreadPriority.Normal, false)
		{
		}		
		
		public Dispatcher Dispatcher
		{
			get { return _dispatcher; }	
			set { _dispatcher = value; }
		}
		
		protected override void Process (IMessage message)
		{
			if(_dispatcher == null)
			{
				throw new InvalidOperationException();
			}
			
			if (message == null)
            {
                throw new ArgumentNullException("message");
            }  
			
			string recepient = message.Header.Recepient;
			MessageProcessor messageProcessor = _dispatcher.GetMessageProcessor(recepient);
            if (messageProcessor == null)
            {
                throw new InvalidOperationException();
            }		
			
			try
            {	
				object messageBody = message.Body;
				if(messageBody is SystemMessage)
				{
					SystemMessage systemMessage = (SystemMessage)messageBody;
					switch (systemMessage) 
					{
						case SystemMessage.Start:
							if(messageProcessor.Status != Status.Started)
							{
								messageProcessor.Start();
							}
							break;
						case SystemMessage.Stop:
							if(messageProcessor.Status != Status.Started)
							{
								messageProcessor.Stop();
							}
							break;
						case SystemMessage.Pause:
							if(messageProcessor.Status != Status.Paused)
							{
								messageProcessor.Pause();
							}
							break;
					}
				}
				else
				{	
					if(messageProcessor.CanAccept(message.Body.GetType()))
					{
						if(messageProcessor.Status == Status.Started)
						{
							messageProcessor.Receive(message);  
						}
						else
						{
							Dispatcher.Enqueue(message);
						}
					} 
					else 
					{
						Console.WriteLine ();
					}
				}
            }
            catch (Exception e)
            {   
				Console.WriteLine (e);
            }
            finally
            {          
            }
		}
	}
}