using System;
using System.Threading;

namespace Daemoniq.Messaging
{
	public abstract class WorkerThread : IDisposable
	{	
		protected internal readonly ManualResetEvent _stopRequested;
		protected internal ManualResetEvent _stopped;
		private Thread _thread;
		private bool _disposed = false;
		private static int _threadCounter=1;
		
		public WorkerThread()
			:this("WorkerThread" + _threadCounter, ThreadPriority.Normal, false)
		{
			_threadCounter++;
		}
		
		public WorkerThread(string name)
			:this(name, ThreadPriority.Normal, false)
		{
		}
		
		public WorkerThread(string name,
		                    ThreadPriority threadPriority, 
		                    bool isBackground)
		{
			if(null == name) throw new ArgumentNullException("name");
			if(0 == name.Length) throw new ArgumentOutOfRangeException("name");
						
			_stopped = new ManualResetEvent(false);
			_stopRequested = new ManualResetEvent(false);
			
			_thread = new Thread(threadProc);
			_thread.Name = name;
			_thread.Priority = threadPriority;
			_thread.IsBackground = isBackground;
		}	
		
		~WorkerThread()
		{
			Dispose(false);
		}
		
		public string Name
		{
			get { return _thread.Name; }
		}
		
		public bool StopRequested
		{
			get{ return _stopRequested.WaitOne(0, true); }
		}
		
		public void Stop()
		{
			_stopRequested.Set();
			_stopped.WaitOne();			
		}
		
		public void Start()
		{		
			_thread.Start();			
		}
		
		public abstract void Work();
		
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		protected virtual void Dispose(bool disposing)
		{
			if(!_disposed)
			{		
				if(disposing)
				{
					//free managed objects
				}
				
				//free unmanaged objects 
				
				//stop the thread
				_stopRequested.Set();
				
				//wait for the thread to exit
				_stopped.WaitOne();
				
				_stopRequested.Close();
				_stopped.Close();
				
				_thread = null;
				_disposed = true;
			}
		}
		
		private void threadProc()
		{
			Work();
			_stopped.Set();
		}
	}
}