using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Daemoniq.Messaging
{
	public abstract class ThreadedQueue<TItem> : WorkerThread
	{
		private AutoResetEvent _itemArrived = new AutoResetEvent(false);
		private Queue<TItem> _itemQueue = new Queue<TItem>(1024);
		private bool _disposed = false;
		private int _itemsReceived = 0;
		private int _itemsProcessed = 0;
		private int _itemsDropped = 0;
		
		public ThreadedQueue()
			:base()
		{
		}
		
		public ThreadedQueue(string name)
			:base(name, ThreadPriority.Normal, false)
		{
		}
		
		public ThreadedQueue(string name,
		                    ThreadPriority threadPriority, 
		                    bool isBackground)
			:base(name, ThreadPriority.Normal, false)
		{			
		}	
		
		public int ItemsReceived
		{
			get { return _itemsReceived; }
		}
		
		public int ItemsProcessed
		{
			get { return _itemsProcessed; }
		}
		
		public int ItemsDropped
		{
			get { return _itemsDropped; }
		}
		
		public new void Stop()
		{
			_stopRequested.Set();
			_itemArrived.Set();
			_stopped.WaitOne();
		}
		
		public override void Work ()
		{
			while(!StopRequested)
			{
				_itemArrived.WaitOne();
				while(_itemQueue.Count > 0)
				{
					lock(((ICollection)_itemQueue).SyncRoot)
					{
						TItem item = _itemQueue.Dequeue();
						if(item != null)
						{
							Process(item);
							Interlocked.Increment(ref _itemsProcessed);
						}
					}
				}
			}
		}
		
		public bool Enqueue(TItem item)
		{
			Interlocked.Increment(ref _itemsReceived);
			
			if(StopRequested)
			{
				Interlocked.Increment(ref _itemsDropped);
				return false;
			}
			
			lock(((ICollection)_itemQueue).SyncRoot)
			{
				_itemQueue.Enqueue(item);
			}
			_itemArrived.Set();
			return true;
		}
				
		protected override void Dispose(bool disposing)
		{
			if(!_disposed)
			{
				if(disposing)
				{
					//free managed objects
				}
				Stop();
				
				_itemQueue.Clear();
				_itemQueue = null;
				
				_itemArrived.Close();	
				
				_disposed = true;
			}
			
			Console.WriteLine ("{0} Received = {1}", Name, _itemsReceived);
			Console.WriteLine ("{0} Processed = {1}", Name, _itemsProcessed);
			Console.WriteLine ("{0} Dropped = {1}", Name, _itemsDropped);
			Console.WriteLine ();
			
			base.Dispose(disposing);
		}		
		
		protected abstract void Process(TItem item);		
	}
}