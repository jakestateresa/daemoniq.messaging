using System;

namespace Daemoniq.Messaging
{
	public class MessageHeader
	{
		//private readonly string _id;
		//private readonly string _correlationId;
		private readonly DateTime _dateCreated = DateTime.UtcNow;
		private readonly string _sender;
		private readonly string _recepient;	
		
		public MessageHeader(string recepient)
			: this(null, recepient)
		{
		}
		
		public MessageHeader(string sender, string recepient)
		{
			if(null == recepient) throw new ArgumentNullException("recepient");
			if(0 == recepient.Length) throw new ArgumentOutOfRangeException("recepient");			
			
			//_id = DateTime.Now.ToString("yyyyMMddHHmmssffffff");//Guid.NewGuid().ToString();
			_sender = sender;
			_recepient = recepient;
		}
		
		public string Id
		{
			get { return _dateCreated.ToString("yyyyMMddHHmmssffffff"); }
		}
		
		public DateTime DateCreated
		{
			get { return _dateCreated; }
		}
		
		public string Sender
		{
			get { return _sender; }
		}
		
		public string Recepient
		{
			get { return _recepient; }
		}
	}
}