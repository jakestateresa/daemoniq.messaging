namespace Daemoniq.Messaging
{
	public enum Status
	{
		Started,
		Stopped,
		Paused
	}
}