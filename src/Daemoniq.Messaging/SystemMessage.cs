namespace Daemoniq.Messaging
{
	enum SystemMessage 
	{
		Start,
		Stop,
		Pause,
	}
}