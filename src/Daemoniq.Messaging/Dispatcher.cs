using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Daemoniq.Messaging
{
	public class Dispatcher:ThreadedQueue<IMessage>
	{	
		private readonly int _numberOfThreadPerProcessor;
		private Dictionary<string, MessageProcessor> _messageProcessors;
		private Queue<DispatcherThread> _dispatcherThreads;
		private bool _disposed = false;
		
		public Dispatcher()
			:this(4)
		{
		}
				
		public Dispatcher(int numberOfThreadsPerProcessor)
			:base("Dispatcher", ThreadPriority.AboveNormal, false)
		{
			if (numberOfThreadsPerProcessor < 1)
            {                
                throw new ArgumentOutOfRangeException("numberOfThreadsPerProcessor");
            }
			
			_numberOfThreadPerProcessor = numberOfThreadsPerProcessor;
			_messageProcessors = new Dictionary<string, MessageProcessor>();
			_dispatcherThreads = new Queue<DispatcherThread>();
			
			int threadCount = _numberOfThreadPerProcessor * Environment.ProcessorCount;
            string dispatcherThreadNameTemplate = "DispatcherThread{0}";            
			for (int i = 1; i <= threadCount; i++)
            {
				string dispatcherThreadName = string.Format(dispatcherThreadNameTemplate, i);
				DispatcherThread dispatcherThread = new DispatcherThread(dispatcherThreadName);
				dispatcherThread.Dispatcher = this;
				_dispatcherThreads.Enqueue(dispatcherThread);	
			}
		}
		
		public int NumberOfThreadsPerProcessor
        {
            get { return _numberOfThreadPerProcessor; }            
        }
		
		public void Register(MessageProcessor messageProcessor)
        {       
            if (messageProcessor == null)
            {
                throw new ArgumentNullException("messageProcessor"); 
            }
            
			string address = messageProcessor.Address;
            if (_messageProcessors.ContainsKey(address))
            {
                throw new InvalidOperationException();
            }

            lock (((ICollection)_messageProcessors).SyncRoot)
            {
                messageProcessor.Dispatcher = this;
				_messageProcessors.Add(address, messageProcessor);
            }
        }     

        public void Unregister(MessageProcessor messageProcessor)
        {            
            if (messageProcessor == null)
            {
                throw new ArgumentNullException("messageProcessor"); 
            }
            
			string address = messageProcessor.Address;
            if (!_messageProcessors.ContainsKey(address))
            {
                throw new InvalidOperationException();
            }

            lock (((ICollection)_messageProcessors).SyncRoot)
            {
				MessageProcessor messageProcessorToRemove = _messageProcessors[address];
				messageProcessorToRemove.Dispatcher = null;
                _messageProcessors.Remove(address);                
            }            
        }   
		
		public MessageProcessor GetMessageProcessor(string address)
        {
            if (address == null)
            {
                throw new ArgumentNullException("address");
            }
            
            if (address.Length == 0)
            {
                throw new ArgumentOutOfRangeException("address");
            }
            
            MessageProcessor messageProcessor = null;
            if (_messageProcessors.ContainsKey(address))
            {
                messageProcessor = _messageProcessors[address];
            }
            return messageProcessor;
        }
		
		public new void Start()
		{
			foreach (DispatcherThread dispatcherThread in _dispatcherThreads) {
				dispatcherThread.Start();
			}
			base.Start();
			
			foreach(string address in _messageProcessors.Keys) {
				Message startMessage = new Message(address, SystemMessage.Start);
				Enqueue(startMessage);
			}
		}
		
		public new void Stop()
		{	
			foreach(string address in _messageProcessors.Keys) 
			{
				Message stopMessage = new Message(address, SystemMessage.Stop);
				Enqueue(stopMessage);
			}
			
			base.Stop();
			foreach (DispatcherThread dispatcherThread in _dispatcherThreads) {
				dispatcherThread.Stop();
			}
		}
		
		protected override void Process (IMessage message)
		{
			if (message == null)
            {
                throw new ArgumentNullException("message");
            }  
			
            if(_dispatcherThreads.Count == 0)
            {
                return;
            }
			
            DispatcherThread dispatcherThread = _dispatcherThreads.Dequeue();
			dispatcherThread.Enqueue(message);	
			_dispatcherThreads.Enqueue(dispatcherThread);                
		}
		
		protected override void Dispose(bool disposing)
        {
			if(!_disposed)
			{
				if(disposing)
				{
					//free managed objects
					
				}
				
				Stop();
				
				//clean-up dispatcher threads
				do
	            {
	                DispatcherThread dispatcherThread = _dispatcherThreads.Dequeue();
					dispatcherThread.Dispatcher = null;
	                dispatcherThread.Dispose();
	                dispatcherThread = null;
	            } while (_dispatcherThreads.Count > 0);

				foreach (MessageProcessor messageProcessor in _messageProcessors.Values)
	            {
					//detach the message processor from the dispatcher
	                messageProcessor.Dispatcher = null;
	
	                if (messageProcessor is IDisposable)
	                {
	                    ((IDisposable)messageProcessor).Dispose();
	                }
	            }
	
	            _messageProcessors.Clear();
				_disposed = true;
			}
            
           	//do some personal hygiene
            base.Dispose(disposing);
        }
	}
}